/*
    Reader
    This is the activity where the actual reader will be displayed.
 */

package com.example.booktester

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient

class Reader : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reader)

        val myPDFReader = findViewById<WebView>(R.id.webViewReader)
        myPDFReader.settings.javaScriptEnabled = true
        myPDFReader.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(
                view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }
        }

        TODO("Need to get the path to the pdf that is passed from the Main Activity")
        //val pdfUrl = "path passed from List View"
        //myPDFReader.loadUrl(pdfUrl)
    }
}