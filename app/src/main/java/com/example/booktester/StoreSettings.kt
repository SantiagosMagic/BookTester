/*
    This class will hold the overall app settings.
 */

package com.example.booktester

object StoreSettings {

    var booksDirectory = "a directory"
    var forcePage1 = "a file"
    var forcePage2 = "another file"
    var forcePage3 = "yet another file"
}