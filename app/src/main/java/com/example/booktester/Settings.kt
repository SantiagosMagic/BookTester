/*
    Settings
    This activity handles the overall settings of the app.
 */

package com.example.booktester

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class Settings : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        // set some local variables from StoreSettings
        val bookDir = StoreSettings.booksDirectory
        val fp1 = StoreSettings.forcePage1
        val fp2 = StoreSettings.forcePage2
        val fp3 = StoreSettings.forcePage3

        // set some local variables from Settings Activity
        val bookRef = findViewById<EditText>(R.id.bookDirecotryEditText)
        val fp1Ref = findViewById<EditText>(R.id.forcePage1EditText)
        val fp2Ref = findViewById<EditText>(R.id.forcePage2EditText)
        val fp3Ref = findViewById<EditText>(R.id.forcePage3EditText)
        val saveSettingsButton = findViewById<Button>(R.id.saveSettingButton)

        bookRef.setText(bookDir)
        fp1Ref.setText(fp1)
        fp2Ref.setText(fp2)
        fp3Ref.setText(fp3)

        saveSettingsButton.setOnClickListener {
            StoreSettings.booksDirectory = bookRef.toString()
            StoreSettings.forcePage1 = fp1.toString()
            StoreSettings.forcePage2 = fp2.toString()
            StoreSettings.forcePage3 = fp3.toString()

            Toast.makeText(this,"Settings Saved", Toast.LENGTH_LONG).show()
        }



    }
}