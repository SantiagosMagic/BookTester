package com.example.booktester

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import java.io.File

class FilesListViewAdapter(private val files: List<File>) : BaseAdapter() {
    override fun getCount(): Int = files.size
    override fun getItem(position: Int): Any = files[position]
    override fun getItemId(position: Int): Long = position.toLong()
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(parent.context)
            .inflate(android.R.layout.simple_list_item_1, parent, false)

        val file = files[position]
        val textView = view.findViewById<TextView>(android.R.id.text1)
        textView.text = file.name

        return view
    }
}