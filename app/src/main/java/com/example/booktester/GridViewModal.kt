/* GridViewModal
    Modal class to handle the Grid View items
 */

package com.example.booktester

class GridViewModal (
    val bookName: String,
    val bookImg: Int
)