/*
    MainActivity
    This activity will be the page to select the book to be read.
 */

package com.example.booktester

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //get the downloads directory and list of PDF's
        val downloadFolder = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
        val listOfFiles = downloadFolder?.listFiles()
                ?.filter{it.isFile &&
                         it.name.endsWith(".pdf", ignoreCase = true)}

        //create an adapter using the files list and populate the listview
        val fileAdapter = listOfFiles?.let { FilesListViewAdapter(it) }
        val refToList = findViewById<ListView>(R.id.bookListView)
        refToList.adapter = fileAdapter

        //set an onclicklistener for the listview
        refToList.onItemClickListener = AdapterView.OnItemClickListener {
            parent, view, position, id ->

            val item = fileAdapter?.getItem(position)
            Toast.makeText(this, "Item clicked: $item", Toast.LENGTH_LONG).show()

            /*
            //uncomment this code when ready to go to the reader
            val intent = Intent(this, Reader::class.java)
                startActivity(intent)
             */
        }

    }
}