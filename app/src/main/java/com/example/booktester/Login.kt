/*
    Login
    This activity will look like a standard "Login" page but is actually how the
    user will be able to access the settings.

    The "Login" will accept all login credentials and open the app but if the "login" is:
    - email: magic@magic.com
    - password: magic
    then the user will be taken to the settings activity to configure how the app works.
 */

package com.example.booktester

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.widget.Button
import android.widget.EditText


class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //local variables
        val emailText = findViewById<EditText>(R.id.loginEmail)
        val passwordText = findViewById<EditText>(R.id.editTextTextPassword)
        val loginButton = findViewById<Button>(R.id.loginButton)

        StoreSettings.booksDirectory = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString()

        loginButton.setOnClickListener{

            if (emailText.text.toString() == "magic@magic.com" &&
                passwordText.text.toString() == "magic") {
                // go to settings activity
                val intent = Intent(this, Settings::class.java)
                startActivity(intent)
            } else {
                // go to Main Activity
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }

        }

    }
}